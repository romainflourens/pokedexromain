import { Component, OnInit } from '@angular/core';
import pokemonData from '../seed.json'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  //variables permettant l'usage du two-way binding directive NgForm
  inputName = '';
  inputId = '';
  sel = '';

  //variable permettant la récupération de la liste d'objets Pokemons depuis le fichier JSON ../seed.json
  pokemons: any[] = pokemonData;

  // variable qui va me permettre de récupérer mes types de pokemons selectionnés dans le filtre Select
  pokemonTypeSelect: any[] = [
    {
      type: "Grass",
    },
    {
      type: "Poison",
    },
    {
      type: "Fire",
    },
    {
      type: "Water",
    },
    {
      type: "Flying",
    },
    {
      type: "Bug",
    },
    {
      type: "Normal",
    },
    {
      type: "Electric",
    },
    {
      type: "Ground",
    },
    {
      type: "Fighting",
    },
    {
      type: "Psychic",
    },
    {
      type: "Rock",
    },
    {
      type: "Steel",
    },
    {
      type: "Ice",
    },
    {
      type: "Ghost",
    },
    {
      type: "Dragon",
    }
  ]

  //à l'init, vérification dans la console de la récupération de la liste des objets pokemons
  ngOnInit() {
    console.log(this.pokemons)

  }
}
