import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pokemonType'
})
export class PokemonTypePipe implements PipeTransform {

  /**
  * Transform
  *
  * @param {any[]} items
  * @param {string} searchText
  * @returns {any[]}
  */

  transform(items: any[], searchText: string): any[] {
    if (!items) {

      return [];
    }
    if (!searchText) {
      return items;
    }

    //fabrique un tableau qui renvoit la liste des pokemons ayant le type selectionné
    return items.filter(it => {
      return (it.type.includes(searchText));
    });
  }
}

