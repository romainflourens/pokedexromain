import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterId'
})
export class FilterIdPipe implements PipeTransform {

  /**
  * Transform
  *
  * @param {any[]} items
  * @param {string} searchText
  * @returns {any[]}
  */
  transform(items: any[], searchText: string): any[] {
    if (!items) {

      return [];
    }
    if (!searchText) {
      return items;
    }

    searchText = searchText.toString().toLocaleLowerCase();
  
    console.log(items);
    console.log(typeof(searchText));
    
    //fabrique un tableau qui retourne que ce qui est filtré
    //NB: les id sont de type string -> parseInt pour transformer les id en Int, ainsi que le searchText
    return items.filter(it => {
      return parseInt (it.id) == parseInt(searchText);
    });
  }
}
