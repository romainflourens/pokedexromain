import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterName'
})
export class FilterNamePipe implements PipeTransform {

  /**
   * Transform
   *
   * @param {any[]} items
   * @param {string} searchText
   * @returns {any[]}
   */
  transform(items: any[], searchText: string): any[] {
    if (!items) {
        
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    console.log(items);

    //fabrique un tableau qui retourne ce qui est inclue dans le champ de recherche (searchText)
    return items.filter(it => {
      return it.name.toLocaleLowerCase().includes(searchText);
    });
  }
}
