import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterNamePipe } from './pipes/filter-name.pipe';
import { FilterIdPipe } from './pipes/filter-id.pipe';
import { PokemonTypePipe } from './pipes/pokemon-type.pipe';


@NgModule({
  declarations: [
    AppComponent,
    FilterNamePipe,
    FilterIdPipe,
    PokemonTypePipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
